// index.js

let studentsNotes = [
	
	"John",
	"Jane",
	"Jeff",
	"Kevin",
	"David"
	
];

console.log(studentsNotes);

class Dog {

	constructor(name,breed,age){

		this.name = name;
		this.breed = breed;
		this.age = age;
	}
}

let dog1 = new Dog ("Bantay","corgi",3);
console.log(dog1);


let person1 = {

		name: "Saitama",
		heroName: "One Punch Man",
		age: 30
};

console.log(person1);

console.log("I am sleepy and cold.");

studentsNotes.push("Henry"); //add item at the end of array
console.log(studentsNotes);
studentsNotes.unshift("Jeremiah"); //add item at the start of array
console.log(studentsNotes);
studentsNotes.unshift("Christine");
console.log(studentsNotes);



let arrNum = [15,25,50,20,10,11];


//forEach()
let allDivisible;
arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5.`)
	} else {
		allDivisible = false;
	}
})

console.log(`forEach(): ${allDivisible}`);

// arrNum.pop(11);
arrNum.push(35);


//every()
let isDivisibleby5 = arrNum.every(num => {

	console.log(num);
	return num % 5 === 0;
	return true
})

console.log(`every(): ${isDivisibleby5}`);



//some()
let someDivisibleBy5 = arrNum.some(num => {
	
	console.log(num);
	return num % 5 === 0

});

console.log(`some(): ${someDivisibleBy5}`);

let someDivisibleBy11 = arrNum.some(num => {
	console.log(num);
	return num % 11 === 0;
})

console.log(`some(): ${someDivisibleBy11}`);

let someDivisibleBy6 = arrNum.some(num => {
	console.log(num);
	return num % 6 === 0;
})

console.log(`some(): ${someDivisibleBy6}`);

/*===========================================================*/

/* QUIZ 

1. array literal "[ ]"
2. by using index array[0]
3. by using index array[array.length-1]
4. indexOf() method
5. forEach() method
6. map() method
7. every() method
8. some() method
9. False
10. True
11. False
12. Math
13. False
14. False
15. True

*/

/*===========================================================*/

/* FUNCTION CODING ACTIVITY */

let students = ["John","Joe","Jane","Jessie"];


//1.

let addToEnd = (array,name) => {
	if(typeof name !== 'string'){
		console.log(`error - can only add strings to an array`);
	} else {
		array.push(name);
		return array
	}
}

//2.

let addToStart = (array,name) => {
	if(typeof name !== 'string'){
		console.log(`error - can only add strings to an array`);
	} else {
		array.unshift(name);
		return array
	}
}

//3.

let elementChecker = (array,name) => {

	if(array.length !== 0){
		let stringy = array.some(student => {
			return array.includes(name);
		});
		console.log(stringy)
	} else {
		return console.log(`error - passed in array is empty`)
	}

}

//4.

let stringLengthSorter = (array) => {

	let stringCheck = array.every(string => {
		return typeof string === 'string'
	})
	if(stringCheck){
		return array.sort()
	} else{
		return console.log(`error - all array elements must be strings`);
	}

}

